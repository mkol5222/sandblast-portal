export function sandblastDownload(downloadID) {
    var req = new XMLHttpRequest;

    var items = {};
    items.working_with_agent = false;
    items.te_use_recommanded_environment = true;
    items.te_cloud_server = "te.checkpoint.com";
    items.te_cloud_api_key = "TE_API_KEY_7iWdn3Pe9hzr3sxpsoNrCyzWwTeuriAJuAuPBlUZ";

    req.responseType = 'blob';
    req.onload = function () {
        if (req.status == 200) {
            console.log("Download SUCCESS", req.responseType);
            var file = window
                .URL
                .createObjectURL(new Blob([req.response], {type: 'application/pdf'}));
            var a = document.createElement("a");
            a.style = "display: none";
            a.href = file;
            a.download = "detailPDF";
            document
                .body
                .appendChild(a);
            a.click();
            // remove `a` following `Save As` dialog, `window` regains `focus`
            window.onfocus = function () {
                document
                    .body
                    .removeChild(a)
            }

        } else {
            console.log("Download ERR", req.response);

        }
    }
    req.ontimeout = function () {
        console.log("sandblastDownload.ontimeout FAILED", req)
    };
    req.onerror = function () {
        console.log("sandblastDownload.onerror FAILED", req)
    };

    req.open("GET", "https://te.checkpoint.com/tecloud/api/v1/file/download?id=" + downloadID, !0);
    req.setRequestHeader("Authorization", items.te_cloud_api_key.split("#")[0]);
    req.setRequestHeader("cache-control", "no-cache");
    req.timeout = 3E4;
    req.send();

}

export function sandblastQuery(a, callbackTarget) {
    // a.file_sha1 a.file_extension a.orig_file_name a.gotResponseCallback
    var b,
        req = new XMLHttpRequest;
    // b = reqJSON
    req.onload = function () {
        200 == req.status
            ? console.log("sandblastQuery.onLoad", req.responseText)
            : console.log("sandblastQuery.onLoad FAILED", req);
        if (req.status == 200) {
            if (a.gotResponseCallback) {
                a.gotResponseCallback(req.response, callbackTarget);
            }
        }
    };

    req.ontimeout = function () {
        console.log("sandblastQuery.ontimeout FAILED", req)
    };
    req.onerror = function () {
        console.log("sandblastQuery.onerror FAILED", req)
    };

    // HARDCODED
    var use_te_cloud_bool = true;
    var items = {};
    items.working_with_agent = false;
    items.te_use_recommanded_environment = true;
    items.te_cloud_server = "te.checkpoint.com";
    items.te_cloud_api_key = "TE_API_KEY_7iWdn3Pe9hzr3sxpsoNrCyzWwTeuriAJuAuPBlUZ";

    use_te_cloud_bool
        ? (b = {
            request: {
                sha1: a.file_sha1,
                file_type: a.file_extension,
                //file_type: "doc", file_name: "aaa.doc",
                file_name: a.orig_file_name,
                features: ["te"],
                te: {
                    reports: ["xml", "pdf"]
                }
            }
        }, items.working_with_agent && (b.request[0].te.reports = ["xml", "pdf"]), !items.te_use_recommanded_environment && 0 < items.te_environment_objects.length && (b.request[0].te.images = items.te_environment_objects), b = JSON.stringify(b), req.withCredentials = !0, req.open("POST", "https://" + items.te_cloud_server + "/tecloud/api/v1/file/query", !0), req.setRequestHeader("Authorization", items.te_cloud_api_key.split("#")[0]), req.setRequestHeader("cache-control", "no-cache"))
        : (b = {
            request: [
                {
                    protocol_version: "1.1",
                    api_key: items.api_key,
                    request_name: "QueryFile",
                    sha1: a.file_sha1,
                    features: ["te"]
                }
            ]
        }, b = JSON.stringify(b), req.open("POST", "https://" + items.server + "/UserCheck/TPAPI", !0));
    req.timeout = 3E4;
    req.send(b)
}

export function sandblastUpload(a, callbackTarget) {
    // a.file_data - ArrayBuffer a.file_sha1 a.file_extension a.orig_file_name
    // HARDCODED
    var use_te_cloud_bool = true;
    var items = {};
    items.working_with_agent = false;
    items.te_use_recommanded_environment = true;
    items.te_cloud_server = "te.checkpoint.com";
    items.te_cloud_api_key = "TE_API_KEY_7iWdn3Pe9hzr3sxpsoNrCyzWwTeuriAJuAuPBlUZ";

    var g = new XMLHttpRequest,
        h = "https://" + items.te_cloud_server + "/tecloud/api/v1/file/upload";

    g.onload = function () {
        200 == g.status
            ? console.log("done", g.responseText)
            : console.log("upload failed", g);
        if (g.status == 200) {
            if (a.gotResponseCallback) {
                a.gotResponseCallback(g.response, callbackTarget);
            }
        }
    };
    g.ontimeout = function () {
        console.log("upload timeout", g);
    };
    g.onerror = function () {
        console.log("Error uploading file", g);
    };
    if (use_te_cloud_bool) {
        var k = {
            request: [
                {
                    //sha1: a.file_sha1, file_type: "doc", file_name: "aaa.doc",

                    file_type: a.file_extension,
                    file_name: a.orig_file_name,
                    sha1: a.file_sha1,
                    features: ["te"],
                    te: {
                        reports: ["xml", "pdf"]
                    }
                }
            ]
        };
        //k.request[0].te.reports = ["xml", "pdf"]; req JSON - b
        var b = JSON.stringify(k);
        k = new FormData;
        k.append("request", b);
        //console.log("a.file_data", a.file_data);
        k.append("file", new Blob([a.file_data], {type: "application/octet-stream"}));
        try {
            g.withCredentials = !0,
            g.open("POST", h, !0),
            g.setRequestHeader("Authorization", items.te_cloud_api_key.split("#")[0]),
            g.setRequestHeader("cache-control", "no-cache"),
            g.timeout = 3E5,
            g.send(k)
        } catch (m) {
            console.log("file upload failed");
        }
    }
}