var React = require('react');

var sha1 = require('js-sha1');

var filesize = require('filesize');

var dateFormat = require('dateformat');

import {sandblastQuery, sandblastUpload, sandblastDownload} from '../../lib/sandblast';
import {fileExt} from "../../lib/misc";

var Spinner = require('react-spinkit');

import {ListGroup, ListGroupItem, ButtonGroup, Button} from 'react-bootstrap';

import {
    Card,
    CardHeader,
    CardTitle,
    CardFooter,
    ListGroupItemHeader,
    ListGroupItemText
} from 'react-bootcards';

var FileItem = React.createClass({
    getInitialState: function () {
        this.getBasicInfo();
        return {verdict: "n/a"};
    },
    upload: function () {
        console.log("upload", this.state.sha1);
        sandblastUpload({
            file_data: this.state.file_content,
            file_sha1: this.state.sha1,
            file_extension: fileExt(this.props.file.name),
            orig_file_name: this.props.file.name,
            gotResponseCallback: function (resp, thisComponent) {
                try {
                    var parsedResponse = JSON.parse(resp);
                    if (Array.isArray(parsedResponse.response)) {
                        parsedResponse.response = parsedResponse.response[0];
                    }
                    console.log("response in callback", parsedResponse);
                    var newState = thisComponent.state;
                    if (!newState) {
                        newState = {}
                    }
                    newState.verdict = parsedResponse.response.te.combined_verdict;
                    newState.serverResponse = parsedResponse;

                    console.log(parsedResponse.response.status.code, parsedResponse.response.status.message);
                    if (parsedResponse.response.status.code == 1002) {
                        // 1002 - uploaded succesfully
                        thisComponent.queryLater();
                        newState.verdict = "Upload successful. Pending investigation"

                    } else if (parsedResponse.response.status.code == 1003) {
                        // 1003 pending
                        thisComponent.queryLater();
                        newState.verdict = "Pending investigation"

                    }
                    thisComponent.setState(newState);
                } catch (k) {
                    console.log("error parsing server response " + k.message);
                }
            }
        }, this);
    },
    query: function () {
        console.log("query", this.state.sha1);
        sandblastQuery({
            //file_data: this.state.file_content,
            file_sha1: this.state.sha1,
            file_extension: fileExt(this.props.file.name),
            orig_file_name: this.props.file.name,
            gotResponseCallback: function (resp, thisComponent) {
                try {
                    var parsedResponse = JSON.parse(resp);
                    if (Array.isArray(parsedResponse.response)) {
                        parsedResponse.response = parsedResponse.response[0];
                    }
                    console.log("response in callback", parsedResponse, this, thisComponent);
                    var newState = thisComponent.state;
                    if (!newState) {
                        newState = {}
                    }
                    newState.verdict = parsedResponse.response.te.combined_verdict;
                    newState.serverResponse = parsedResponse;
                    thisComponent.setState(newState);
                    console.log(parsedResponse.response.status.code, parsedResponse.response.status.message);
                    if (parsedResponse.response.status.code == 1004) {
                        // 1004 - not found, upload
                        thisComponent.upload();
                        newState.verdict = "Uploading";

                    } else if (parsedResponse.response.status.code == 1003) {
                        // 1003 pending
                        thisComponent.queryLater();
                        newState.verdict = "Pending investigation";
                    }
                    thisComponent.setState(newState);
                } catch (k) {
                    console.log("error parsing server response " + k.message);
                }
            }
        }, this);
    },
    queryLater: function () {
        this.nextTimeout = setTimeout(this.query, 30 * 1000);
    },
    componentWillUnmount: function () {
        clearTimeout(this.nextTimeout);
    },
    onButtonClick: function (item, event) {
        sandblastDownload(item);
        console.log("onButtonClick", item, event);
    },
    getBasicInfo: function () {
        var f = this.props.file;
        var reader = new FileReader();

        // Closure to capture the file information.
        reader.onload = (function (theFile, fileItemComponent) {

            return function (e) {
                console.log("fileItemComponent", fileItemComponent);
                var fileSHA1 = sha1(e.target.result);

                var newState = fileItemComponent.state;
                if (!newState) {
                    newState = {}
                }
                newState.sha1 = fileSHA1;
                newState.file_content = e.target.result;

                fileItemComponent.setState(newState, fileItemComponent.query);

                //fileItemComponent.query();

            }

        })(f, this);

        // Read in the image file as a data URL.
        reader.readAsArrayBuffer(f);
    },
    render: function () {
        console.log("rendering FileItem", this.props);
        //console.log(this, this.props);
        var fsize = filesize(this.props.file.size);
        var fext = fileExt(this.props.file.name).toUpperCase();
        var fdate = dateFormat(this.props.file.lastModifiedDate, "dddd, mmmm dS, yyyy, h:MM:ss TT");
        var status_message = "";
        // var reports = this     .state     .parsedResponse     .response     .te
        // .images     .map(function (a) {         return (             <li>     rep PDF
        //             </li>         )     });
        var reports = [];
        var showProgress = true;

        var onReportClick = this.onButtonClick;
        try {
            status_message = this.state.serverResponse.response.status.message;
            var status_code = this.state.serverResponse.response.status.code;

            if (status_code == 1006) 
                showProgress = false;
            
            if (status_code == 1001) 
                showProgress = false;
            
            for (var i = 0, image; image = this.state.serverResponse.response.te.images[i]; i++) {
                try {
                    var downloadID = image.report.pdf_report;
                    console.log("report", downloadID);
                    if (downloadID) {
                        reports.push(downloadID);
                    }
                } catch (k) {
                    console.log(k.message);
                }
            }

        } catch (k) {
            console.log(k.message);
        }

        return (
            <Card>
                <ListGroup>
                    <ListGroupItem>
                        <a href="#">
                            <i className="icon-file-pdf"/>
                        </a>
                        <ListGroupItemHeader>
                            <a href="#">{this.props.file.name}</a>
                        </ListGroupItemHeader>
                        <ListGroupItemText>
                            <strong>{fext}</strong>
                        </ListGroupItemText>
                        < ListGroupItemText >
                            <strong>{fsize}</strong>
                        </ListGroupItemText>
                        <ListGroupItemText>
                            <strong>SHA1 {this.state.sha1}</strong>
                        </ListGroupItemText>
                    </ListGroupItem>
                    <ListGroupItem>
                        <ListGroupItemText>
                            <strong>Last modified {fdate}</strong>
                        </ListGroupItemText>
                    </ListGroupItem>
                    <ListGroupItem>
                        <ListGroupItemText>
                            <strong>{showProgress && <Spinner spinnerName='three-bounce'/>} {this.state.verdict}</strong>
                        </ListGroupItemText>
                    </ListGroupItem>
                    {status_code != 1006  && false && <ListGroupItem>
                        <ListGroupItemText>
                            <strong>{status_message}</strong>
                        </ListGroupItemText>
                    </ListGroupItem>
}

                    <ListGroupItem>

                        {reports
                            .map(function (a) {
                                let boundItemClick = onReportClick.bind(this, a);
                                //return <li>{a.file.name} - {a.verdict}</li>;
                                return <Button
                                    bsStyle="success"
                                    bsSize="small"
                                    onClick={boundItemClick}
                                    data-downloadid={a}>
                                    PDF
                                </Button>

                            })}
                    </ListGroupItem>
                </ListGroup>
            </Card>
        )
    }
});

module.exports = FileItem;