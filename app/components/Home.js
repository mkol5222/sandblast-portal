import {hello, clog, isFileAPISupprted, base64ArrayBuffer, fileExt} from "../../lib/misc";

var React = require('react');

var Clock = require("./Clock");
var Dropzone = require('react-dropzone');

var FileItem = require("./FileItem");

import SHA1 from 'sha1-es';
import {sandblastQuery, sandblastUpload} from '../../lib/sandblast';

//require("script-loader!../../lib/cryptoJS-sha1.js");
var sha1 = require('js-sha1');

var Home = React.createClass({
    getInitialState: function () {
        return {files: []};
    },
    onDrop: function (files) {
        console.log('Received files: ', files);
        hello();
        clog("hello");
        isFileAPISupprted();

        var allFiles = this.state.files;
        if (!allFiles) {
            allFiles = [];
        }
        // Loop through the FileList and render image files as thumbnails.
        for (var i = 0, f; f = files[i]; i++) {

            allFiles.push({ file: f, verdict: "N/A" });

            console.log("file", f);
            var reader = new FileReader();

            // Closure to capture the file information.
            reader.onload = (function (theFile) {
                return function (e) {
                    // The file's text will be printed here console.log(e.target.result); var b =
                    // CryptoJS.lib.WordArray.create(a.fileData.orig_file); a.file_sha1 =
                    // CryptoJS.SHA1(b).toString(CryptoJS.enc.Hex)
                    // console.log(base64ArrayBuffer(e.target.result));

                    var fileSHA1 = sha1(e.target.result);
                    console.log(fileSHA1);
                    // const hash = SHA1.hash(e.target.result); console.log(hash); sandblastQuery({
                    // file_sha1: fileSHA1 });

                    sandblastUpload({
                        file_data: e.target.result,
                        file_sha1: fileSHA1,
                        file_extension: fileExt(theFile.name),
                        orig_file_name: theFile.name
                    });

                    //clog("e", e); clog("theFile", theFile);
                };
            })(f);

            // Read in the image file as a data URL.
            //reader.readAsArrayBuffer(f);
        }
        console.log("new state", {files: allFiles});
        this.setState({files: allFiles});
    },

    render: function () { //console.log(this, this.props);
        return (
            <div className="row">
                <div className="col-sm-4">
                    <Dropzone onDrop={this.onDrop}>
                        <div>Try dropping some files here, or click to select files to upload.</div>
                    </Dropzone>
                </div>
                <div className="col-sm-8">
                    <ul>
                        {this
                            .state
                            .files
                            .map(function (a) {
                                //return <li>{a.file.name} - {a.verdict}</li>;
                                return <FileItem file={a.file} />
                            })}
                    </ul>
                </div>
            </div>
        )
    }
});

module.exports = Home;