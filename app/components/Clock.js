var React = require('react');

function currentTimeString() {
    return new Date().toLocaleTimeString();
}

var Clock = React.createClass({
    getInitialState: function(){
        return {
            timeString: currentTimeString()
        };
    },
    tick() {
        this.setState({
            timeString: currentTimeString()
        });
    },
    render: function() {
        //console.log(this, this.props);
        return (
            <div className="text-center">
                Time is <b> {this.state.timeString} </b>
            </div>
        )
    },
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            2000
        );
    },
     componentWillUnmount() {
        clearInterval(this.timerID);
    }
    
});

module.exports = Clock;